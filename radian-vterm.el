;;; radian-vterm.el --- A mode for R REPL using vterm -*- lexical-binding: t -*-

;; Copyright (C) 2020 Shigeaki Nishina

;; Author: Shigeaki Nishina
;; Maintainer: Shigeaki Nishina
;; Created: March 11, 2020
;; URL: https://github.com/shg/radian-vterm.el
;; Package-Requires: ((emacs "25.1") (vterm "0.0.1"))
;; Version: 0.13
;; Keywords: languages, R

;; This file is not part of GNU Emacs.

;;; License:

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or (at
;; your option) any later version.
;;
;; This program is distributed in the hope that it will be useful, but
;; WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;; General Public License for more details.
;;
;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see https://www.gnu.org/licenses/.

;;; Commentary:

;; Provides a major-mode for inferior R process that runs in vterm, and a
;; minor-mode that extends R-mode to support interaction with the inferior
;; R process.

;;; Usage:

;; You must have R-mode and vterm installed.
;; Install radian-vterm.el manually using package.el
;;
;;   (package-install-file "/path-to-download-dir/radian-vterm.el")
;;
;; Eval the following line. Add this line to your init file to enable this
;; mode in future sessions.
;;
;;   (add-hook 'R-mode-hook #'radian-vterm-mode)
;;
;; Now you can interact with an inferior R REPL from a R buffer.
;;
;; C-c C-z in a R-mode buffer to open an inferior R REPL buffer.
;; C-c C-z in the REPL buffer to switch back to the script buffer.
;; C-<return> in the script buffer to send region or current line to REPL.
;;
;; See the code below for a few more key bidindings.

;;; Code:

(require 'vterm)


;;----------------------------------------------------------------------
(defgroup radian-vterm-repl nil
  "A major mode for inferior R REPL."
  :group 'R)

(defvar radian-vterm-repl-program "radian")

(defvar-local radian-vterm-repl-script-buffer nil)

(defvar radian-vterm-repl-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-z") #'radian-vterm-repl-switch-to-script-buffer)
    (define-key map (kbd "M-k") #'radian-vterm-repl-clear-buffer)
    (define-key map (kbd "C-c C-t") #'radian-vterm-repl-copy-mode)
    (define-key map (kbd "C-l") #'recenter-top-bottom)
    map))

(define-derived-mode radian-vterm-repl-mode vterm-mode "Inf-R"
  "A major mode for inferior R REPL."
  :group 'radian-vterm-repl)

(defun radian-vterm-repl-buffer-name (&optional session-name)
  "Return a R REPL buffer name whose session name is SESSION-NAME."
  (format "*R:%s*" (if session-name session-name "main")))

(defun radian-vterm-repl-session-name (repl-buffer)
  "Return the session name of REPL-BUFFER."
  (let ((bn (buffer-name repl-buffer)))
    (if (string= (substring bn 1 7) "R:")
        (substring bn 7 -1)
      nil)))

(defun radian-vterm-repl-buffer-with-session-name (session-name &optional restart)
  "Return an inferior R REPL buffer of the session name SESSION-NAME.
If there exists no such buffer, one is created and returned.
With non-nil RESTART, the existing buffer will be killed and
recreated."
  (if-let ((buffer (get-buffer (radian-vterm-repl-buffer-name session-name)))
           (alive (vterm-check-proc buffer))
           (no-restart (not restart)))
      buffer
    (if (get-buffer-process buffer) (delete-process buffer))
    (if buffer (kill-buffer buffer))
    (let ((buffer (generate-new-buffer (radian-vterm-repl-buffer-name session-name)))
          (vterm-shell radian-vterm-repl-program))
      (with-current-buffer buffer
        (radian-vterm-repl-mode))
      buffer)))

(defun radian-vterm-repl-buffer (&optional session-name restart)
  "Return an inferior R REPL buffer.
The main REPL buffer will be returned if SESSION-NAME is not
given.  If non-nil RESTART is given, the REPL buffer will be
recreated even when a process is alive and running in the buffer."
  (if session-name
      (radian-vterm-repl-buffer-with-session-name session-name restart)
    (radian-vterm-repl-buffer-with-session-name "main" restart)))

(defun radian-vterm-repl ()
  "Create an inferior R REPL buffer `*R:main*` and open it.
If there's already one with the process alive, just open it."
  (interactive)
  (pop-to-buffer-same-window (radian-vterm-repl-buffer)))

(defun radian-vterm-repl-switch-to-script-buffer ()
  "Switch to the script buffer that is paired with this R REPL buffer."
  (interactive)
  (let ((repl-buffer (current-buffer))
        (script-buffer (if (buffer-live-p radian-vterm-repl-script-buffer)
                           radian-vterm-repl-script-buffer
                         nil)))
    (if script-buffer
        (with-current-buffer script-buffer
          (setq radian-vterm-fellow-repl-buffer repl-buffer)
          (switch-to-buffer-other-window script-buffer)))))

(defun radian-vterm-repl-clear-buffer ()
  "Clear the content of the R REPL buffer."
  (interactive)
  (save-excursion
    (goto-char (point-min))
    (vterm-clear 1)))

(defvar radian-vterm-repl-copy-mode-map
  (let ((map (make-sparse-keymap)))
    (define-key map (kbd "C-c C-t") #'radian-vterm-repl-copy-mode)
    (define-key map [return] #'radian-vterm-repl-copy-mode-done)
    (define-key map (kbd "RET") #'radian-vterm-repl-copy-mode-done)
    (define-key map (kbd "C-c C-r") #'vterm-reset-cursor-point)
    map))

(define-minor-mode radian-vterm-repl-copy-mode
  "Toggle copy mode."
  :group 'radian-vterm-repl
  :lighter " VTermCopy"
  :keymap radian-vterm-repl-copy-mode-map
  (if radian-vterm-repl-copy-mode
      (progn
        (message "Start copy mode")
        (use-local-map nil)
        (vterm-send-stop))
    (vterm-reset-cursor-point)
    (use-local-map radian-vterm-repl-mode-map)
    (vterm-send-start)
    (message "End copy mode")))

(defun radian-vterm-repl-copy-mode-done ()
  "Save the active region to the kill ring and exit copy mode."
  (interactive)
  (if (region-active-p)
      (kill-ring-save (region-beginning) (region-end))
    (user-error "No active region"))
  (radian-vterm-repl-copy-mode -1))


;;----------------------------------------------------------------------
(defgroup radian-vterm nil
  "A minor mode to interact with an inferior R REPL."
  :group 'R)

(defcustom radian-vterm-hook nil
  "Hook run after starting a R script buffer with an inferior R REPL."
  :type 'hook
  :group 'radian-vterm)

(defvar-local radian-vterm-fellow-repl-buffer nil)

(defun radian-vterm-fellow-repl-buffer (&optional session-name)
  "Return the paired REPL buffer or the one specified with SESSION-NAME."
  (if session-name
      (radian-vterm-repl-buffer session-name)
    (if (buffer-live-p radian-vterm-fellow-repl-buffer)
        radian-vterm-fellow-repl-buffer
      (radian-vterm-repl-buffer))))

(defun radian-vterm-switch-to-repl-buffer (&optional prefix)
  "Switch to the paired REPL buffer or to the one with a specified session name.
With PREFIX, prompt for session name."
  (interactive "P")
  (let* ((session-name
          (cond ((null prefix) nil)
                (t (read-from-minibuffer "Session name: "))))
         (script-buffer (current-buffer))
         (repl-buffer (radian-vterm-fellow-repl-buffer session-name)))
    (setq radian-vterm-fellow-repl-buffer repl-buffer )
    (with-current-buffer repl-buffer
      (setq radian-vterm-repl-script-buffer script-buffer)
      (switch-to-buffer-other-window repl-buffer))))

(defun radian-vterm-send-return-key ()
  "Send a return key to the R REPL."
  (with-current-buffer (radian-vterm-fellow-repl-buffer)
    (vterm-send-return)))

(defun radian-vterm-paste-string (string &optional session-name)
  "Send STRING to the R REPL buffer using brackted paste mode."
  (with-current-buffer (radian-vterm-fellow-repl-buffer session-name)
    (vterm-send-string string t)))

(defun radian-vterm-send-current-line ()
  "Send the current line to the R REPL, and move to the next line.
This sends a newline after the content of the current line even if there's no
newline at the end.  A newline is also inserted after the current line of the
script buffer."
  (interactive)
  (save-excursion
    (end-of-line)
    (let ((clmn (current-column))
          (char (char-after))
          (line (string-trim (thing-at-point 'line t))))
      (unless (and (zerop clmn) char)
        (when (/= 0 clmn)
          (radian-vterm-paste-string line)
          (radian-vterm-send-return-key)
          (if (not char)
              (newline))))))
  (forward-line))

(defun radian-vterm-send-region-or-current-line ()
  "Send the content of the region if the region is active, or send the current line."
  (interactive)
  (if (use-region-p)
      (progn
        (radian-vterm-paste-string
         (buffer-substring-no-properties (region-beginning) (region-end)))
        (deactivate-mark))
    (radian-vterm-send-current-line)))

(defun radian-vterm-send-buffer ()
  "Send the whole content of the script buffer to the R REPL line by line."
  (interactive)
  (save-excursion
    (radian-vterm-paste-string (buffer-string))))

(defun radian-vterm-send-include-buffer-file (&optional arg)
  "Send a line to evaluate the buffer's file using include() to the R REPL.
With a prefix argument ARG (or interactively C-u), use Revise.includet() instead."
  (interactive "P")
  (let ((fmt (if arg "Revise.includet(\"%s\")\n" "include(\"%s\")\n")))
    (if (and buffer-file-name
             (file-exists-p buffer-file-name)
             (not (buffer-modified-p)))
        (radian-vterm-paste-string (format fmt buffer-file-name))
      (message "The buffer must be saved in a file to include."))))

(defun radian-vterm-send-cd-to-buffer-directory ()
  "Send cd() function call to the R REPL to change the current working directory of REPL to the buffer's directory."
  (interactive)
  (if buffer-file-name
      (let ((buffer-directory (file-name-directory buffer-file-name)))
        (radian-vterm-paste-string (format "cd(\"%s\")\n" buffer-directory))
        (with-current-buffer (radian-vterm-fellow-repl-buffer)
          (setq default-directory buffer-directory)))
    (message "The buffer is not associated with a directory.")))

(defalias 'radian-vterm-sync-wd 'radian-vterm-send-cd-to-buffer-directory)

(unless (fboundp 'R)
  (defalias 'R 'radian-vterm-repl))

;;;###autoload
(define-minor-mode radian-vterm-mode
  "A minor mode for a R script buffer that interacts with an inferior R REPL."
  nil "⁂"
  `((,(kbd "C-c C-z") . radian-vterm-switch-to-repl-buffer)
    (,(kbd "C-<return>") . radian-vterm-send-region-or-current-line)
    (,(kbd "C-c C-b") . radian-vterm-send-buffer)
    (,(kbd "C-c C-i") . radian-vterm-send-include-buffer-file)
    (,(kbd "C-c C-d") . radian-vterm-send-cd-to-buffer-directory)))

(provide 'radian-vterm)

;;; radian-vterm.el ends here
